
# coding: utf-8

# 數列中兩數最大相乘: Find Max pair product

# In[ ]:

temp = input()
result = 0
a = [int(x) for x in input().split()]

for i in range(0,len(a)-1):
    if a[i]>a[i+1]:
        temp = a[i+1]
        a[i+1] = a[i]
        a[i] = temp
        
for j in range(0,len(a)-2):
    if (a[len(a)-1] - a[j])<(a[len(a)-1] - a[j+1]):
        temp = a[j+1]
        a[j+1] = a[j]
        a[j] = temp

print (a[len(a)-1]*a[len(a)-2])


# Task. Given an integer 𝑛,  nd the 𝑛th Fibonacci number 𝐹𝑛.

# In[253]:

# 列出所有Fn
def fib_list(n):
    a = 0
    b = 1
    f = [1]
    d = int(n/2)
    while d:
        a = b + a
        f.append(a)
        b = a + b
        f.append(b)

        d -= 1
        
    print(f[n-1])


# In[254]:

# 找到特定位置Fn
def fib(n):
    a = 0
    b = 1
    d = int(n/2)
    while d:
        a = b + a
        b = a + b
        d -= 1
    if n%2==0:    
        print(a)
    else:
        print(b)


# In[296]:

def find_bigest(num):
    l = []
    l = list(str(num))
    l.sort()
    l.reverse()
    print(l[0])


# In[337]:

fib(238)


# In[338]:

fib(239)


# In[339]:

fib(240)


# 2 Problem: Last Digit of a Large Fibonacci Number
# Problem Introduction
# Your goal in this problem is to  nd the last digit of 𝑛-th Fibonacci number. Recall that Fibonacci numbers grow exponentially fast. For example,

# In[334]:

def fib(n):
    a = 0
    b = 1
    d = int(n/2)
    while d:
        a = b + a
        b = a + b
        d -= 1
    if n%2==0:    
        return a
    else:
        return b
        
def find_bigest(num):
    l = []
    l = list(str(num))
    l.reverse()
    print(l[0])
    
n = int(input())
num = fib(n)
find_bigest(num)

### 失敗，不需要列出整的答案，他只要找最後一個數


# In[360]:

def fib_last(n):
    a = 0
    b = 1
    d = int(n/2)
    while d:
        a = b%10 + a%10
        b = a%10 + b%10
        d -= 1

        
    if n%2==0:    
        print(a%10)
    else:
        print(b%10)


# In[363]:

num = int(input())
fib_last(num)


# 因數分解(not a good algorithm)

# In[174]:

def dis(num):
    l = []
    for n in (range(2,num)):
        if num % n == 0:
            num = num/n
            l.append(n)
    return l


# In[217]:

dis(357)


# In[219]:

dis(234)


# 3 Problem: Greatest Common Divisor
# Problem Introduction
# The greatest common divisor GCD(𝑎,𝑏) of two non-negative integers 𝑎 and 𝑏 (which are not both equal to 0) is the greatest integer 𝑑 that divides both 𝑎 and 𝑏. Your goal in this problem is to implement the Euclidean algorithm for computing the greatest common divisor.
# E cient algorithm for computing the greatest common divisor is an important basic primitive of commonly used cryptographic algorithms like RSA.

# In[378]:

def EucidGCD(num1,num2):
    result = 0

    while num2!=0: 
        result = num2
        num1 = num1%num2
        num2 = num1
        num1 = result
    print(num1)


# In[392]:

num = [int(x) for x in input().split()]
num1 = num[0]
num2 = num[1]
EucidGCD(num1,num2)


# 4 Problem: Least Common Multiple
# Problem Introduction
# The least common multiple of two positive integers 𝑎 and 𝑏 is the least positive integer 𝑚 that is divisible by both 𝑎 and 𝑏

# In[400]:

def EucidGCD(num1,num2):
    result = 0

    while num2!=0: 
        result = num2
        num1 = num1%num2
        num2 = num1
        num1 = result
    return(num1)

num = [int(x) for x in input().split()]
num1 = num[0]
num2 = num[1]
EucidGCD(num1,num2)
print(int((num1*num2)//(EucidGCD(num1,num2))))


# 5 Advanced Problem: Huge Fibonacci Number modulo m
# We strongly recommend you start solving advanced problems only when you are done with the basic problems (for some advanced problems, algorithms are not covered in the video lectures and require additional ideas to be solved; for some other advanced problems, algorithms are covered in the lectures, but implementing them is a more challenging task than for other problems).

# In[579]:

input_n = [int(x) for x in input().split()]
n = input_n[0]
mod = input_n[1]

def find_period_reminder(n): #Find the circle len of reminders
    a = 0 #0
    b = 1 #1
    num = 1
    
    while num:
        c = (a + b) % n #2 3 4 5 ...
        a = b
        b = c
        if a == 0 and b ==1:
            return num
        else:
            num += 1

def fib(n):  #Find a specific Fibonacci
    a = 0
    b = 1
    d = int(n/2)
    while d:
        a = b + a
        b = a + b
        d -= 1
    if n%2==0:    
        return a
    else:
        return b

p = find_period_reminder(mod)  # Find out the period


def find_reminder (n): #shorten the F
    print ((fib(n%p))%mod)


find_reminder(n)


# In[572]:

def find_reminder (n):  # F2015 % 3 = F7 %3  因為2015=251*8 [%3的循環長度] + 7
    print ((fib(n%p))%mod)


# 6 Advanced Problem: Sum of Fibonacci Numbers
# We strongly recommend you start solving advanced problems only when you are done with the basic problems (for some advanced problems, algorithms are not covered in the video lectures and require additional ideas to be solved; for some other advanced problems, algorithms are covered in the lectures, but implementing them is a more challenging task than for other problems).

# In[582]:

def fib_last(n):
    a = 0
    b = 1
    d = int(n/2)
    while d:
        a = b%10 + a%10
        b = a%10 + b%10
        d -= 1

        
    if n%2==0:    
        return(a%10)
    else:
        return(b%10)


# In[583]:

fib_last(1)


# In[598]:

def sum_fib_last(num):
    sum_fib = 0
    for n in range(1,num+1):
        sum_fib += fib_last(n)
    print (sum_fib%10)


# In[603]:

sum_fib_last(10) ## 失敗，太費時


# In[607]:

def sum_fib(n):
    a =1
    b =1
    c = 1
    e = 0
    if n == 0:
        return a
    while n-1:

        c = a + b
        a = c
        
        e = b
        b = b + e
        
        b2 = d1 + b1
        n -= 1
    return a

###思考中


# In[608]:

sum_fib(3)


# In[ ]:



