
# coding: utf-8

# 1. 有一個列表，其中包括 10 個元素，例如這個列表是 [1, 2, 3, 4, 5, 6, 7, 8, 9, 0],要求將列表中的每個元素一次向前移動一 個位置，第一個元素到列表的最後，然後輸出這個列表。最終樣 式是 [2, 3, 4, 5, 6, 7, 8, 9, 0, 1]

# In[23]:

l1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]


# In[26]:

def change_list(l):
    a = l1[0] 
    l.remove(l1[0])
    l.append(a)
    return l


# In[27]:

change_list(l1)


# 2.
# 在數學中，用 n! 來表示階乘，例如，4! =1×2×3×4 =24。 請計算 3!, 4!, 5!。請寫一個程式，讓使用者輸入 n，印出 n!

# In[27]:

def cout_factoral(num):
    k = [str(n)+'x'for n in range(1,num)] 
    print ("".join(k)+str(num))
    
    a = 1
    for n in range(1,num+1):
        a = a*n
    print (a) 


# In[26]:

cout_factoral(8)


# 3.
# 如果將⼀句話作為⼀個字符串，那麼這個字符串中必然 會有空格（這裡只是討論英⽂），⽐如“How are you?”，但 有的時候，會在兩個單詞之間多⼤⼀個空格。現在的任務是，如 果⼀個字符串中有連續的兩個空格，請把它刪除

# In[38]:

def delete_the_space(pharse):
    print (" ".join(pharse.split()))


# In[39]:

pharse = 'How  are you  today'
delete_the_space(pharse)


# 4.
# "Here are UPPERCASE and lowercase chars."
# #{'c': ['c', 'c'], 'R': ['R'], 'w': ['w'], ' ': [' ', ' ', ' ', ' ', ' '], '.': ['.'], 'n': ['n'], 'H': ['H'], 'P': ['P', 'P'], 'h': ['h'], 'S': ['S'], 'e': ['e', 'e', 'e', 'e', 'e'], 'l': ['l'], 'E': ['E', 'E'], 'U': ['U'], 'a': ['a', 'a', 'a', 'a'], 'A': ['A'], 'o': ['o'], 'C': ['C'], 'r': ['r', 'r', 'r', 'r'], 'd': ['d'], 's': ['s', 's']}

# ##### 不太會

# 5.
# 有兩個列表，分別是：a = [1,2,3,4,5]，b = [9,8,7,6,5]， 要計算這兩個列表中對應元素的和

# In[103]:

def sum_in_list(a,b):
    print_list = [i*j for (i,j) in zip(a,b)]
    return print_list


# In[104]:

a = [1,2,3,4,5]
b = [9,8,7,6,5]

sum_in_list(a,b)


# 6.
# 有⼀個字典 ，myinfor = {"name":"qiwsir","site":"qiwsir.github.io","lang":"python"}, 將這 個字典變換成 ：infor = {"qiwsir":"name","qiwsir.github.io":"site","python":"lang"} 

# In[148]:

myinfor = {"name":"qiwsir","site":"qiwsir.github.io","lang":"python"}


# In[152]:

infor = {value : key for key,value in myinfor.items()}
infor
# 如何排序，Dictionary沒有順序


# 7.
# 按照下⾯的要求實現對列表的操作，產⽣⼀個列表，其 中有40個元素，每個元素是0到100的⼀個隨機整數如果這個列表 中的數據代表著某個班級40⼈的分數，請計算成績低於平均分的 學⽣⼈數，並輸出對上⾯的列表元素從⼤到⼩排序

# In[189]:

import random


# In[262]:

def score_list(num):
    score_add = 0
    score_list = []
    value = list(range(101))
    score = random.sample(value,num)
    for s in score:
        score_add += s
        score_list.append(s)
    socre_sort = score_list.sort()
    print ('There are %s students in class'%(num))
    print ('Average Score is %s'%(score_add/num))
    print (score_list)


# In[263]:

score_list(40)


# 承上題，想要對分數進⾏調整，不及格的⼈將原始分數 開根號乘以⼗（但最⾼不可以超過 60 分），及格者不變

# In[300]:

def score_list_change(num):
    score_add = 0
    score_list = []
    value = list(range(101))
    score = random.sample(value,num)
    for s in score:
        if s >= 60:
            score_list.append(s)
            score_add += s
            continue
        else:
            s = (s**(1/2))*10
            if s >= 60:
                s = 60
                score_list.append(s)
                score_add += s
            else:
                score_list.append(s)
                score_add += s
    socre_sort = score_list.sort()
    print ('There are %s students in class'%(num))
    print ('Average Score is %s'%(score_add/num))
    print (score_list)


# In[301]:

score_list_change(40)


# 8.
# 寫⼀個計算平⽅的函式

# In[153]:

def double(num):
    return num**2


# In[154]:

double(123)


# 寫⼀個計算平⽅根的函式

# In[156]:

def Square(num):
    return num**(1/2)


# In[157]:

Square(144)


# 寫⼀個回傳⼀次⽅，⼆次⽅，…，到指定次⽅的函式

# In[159]:

def fun_send_back(num,time):
    for n in range(1,time+1):
        print (num**n)


# In[160]:

fun_send_back(10,5)


# 9.
# 每次考試之後，教師都要統計考試成績，⼀般包括：平 均分，對所有⼈按成績從⾼到低排隊，誰成績最好，誰成績最差 還有其它的統計項，暫且不 為了簡化，以字典形式表⽰考試成績 記錄，例如：{“zhangsan”：90，“lisi” ：78， “wangermazi”：39}，當然，也許不能這三項，可能還有， 每個⽼師所在處理的內容稍有不同，因此字典裡的鍵值對也不⼀ 樣。怎麼做？有幾種可能要考慮到：最⾼分或者最低分，可能有 ⼈並列。要實現不同⾧度的字典作為輸⼊值。輸出結果中，除了 平均分，其它的都要有姓名和分數兩項，否則都匿名了，怎麼刺 激學渣，表揚學霸呢

# In[ ]:




# In[ ]:




# 10.
# 建⽴⼀個 Student 的類別，要包含姓名、性別、年齡， 也要可以修改資料。 

# In[182]:

class Student(object):
    def __init__ (self,name,age,grade):
        self.name = name
        self.age = age
        self.grade = grade
    def graded(self):
        if self.grade >= 80:
            print (self.name + ', you did a greart job')
        else:
            print ('go home! and study')


# In[187]:

k = Student("Kevin",18,83)


# In[188]:

k.graded()


# 11.利⽤上述建⽴的類別，宣告幾個物件，並放到⼀個 dict 保存

# In[ ]:



